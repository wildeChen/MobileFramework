/**
 * Created by wildeChen on 2016/11/23.
 */
define(['zepto', 'page'], function ($, page) {
    var self = new page('fantasy');

    $.extend(self, {
        onEnter: function () {
        },
        testFn: function () {
            console.log(123);
        },
        testFn2: function () {
            console.log(45678);
        }
    });

    return self;
});