/**
 * Created by wildeChen on 2016/11/23.
 */
define(['zepto', 'page'], function ($, page) {
    var self = new page('home');

    $.extend(self, {
        onEnter: function () {
        },
        onExit:function () {
            setTimeout(function () {
                self.exit.resolve()
            },1000)
        },
        testFn: function () {
            console.log(123);
        },
        testFn2: function () {
            console.log(45678);
        }
    });

    return self;
});