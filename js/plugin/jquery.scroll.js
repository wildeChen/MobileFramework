;(function ($) {
    $.fn.scrollLoop = function (vars) {

        var defaults = {
            //定义默认参数
            duration: 2000, //单次滚动快慢 //单位毫秒
            interval: 3000, //单次滚动间隔时间 //单位毫秒
            axis: 'up' //滚动方向 //up or down

        };

        var options = $.extend(defaults, vars);

        if (options.interval <= options.duration) options.interval = options.duration + 1000; //这一段是使用setInterval()时候的事件保护,防止动画崩溃

        function nc_scroll(obj) {

            var self = this; //用来执行内部方法
            var $tag = obj.find('ul'); //找到传入的原件下的互动层
            var moveTarget; //定义一个用来保存移动目标，在暂停后进行续传的参数
            var timer;
            obj.hover( //绑定hover事件
                function () {
                    self.stop();
                },
                function () {
                    self.play();
                }
            );

            this.play = function () {

                var _up = function () {

                    var tagLi = $tag.find('li:first');
                    var tarNum = parseInt(tagLi.height() + tagLi.pxToNum('padding-top') + tagLi.pxToNum('padding-bottom'));
                    var moveTime = options.duration;
                    if (moveTarget === '' || moveTarget === null || moveTarget === undefined) {
                        moveTarget = $tag.pxToNum('margin-top') - tarNum + 'px'
                    } else {
                        var newTagNum = moveTarget.substring(0, moveTarget.length - 2);
                        moveTime = options.duration / tarNum * Math.abs($tag.pxToNum('margin-top') - newTagNum);
                    }
                    $tag.animate({
                            marginTop: moveTarget //滚动事件
                        }, moveTime,
                        function () {
                            //滚动后移动被滚出屏幕的li
                            tagLi.appendTo($tag);
                            var tempDis = $tag.pxToNum('margin-top');
                            var tempNum = tempDis + tarNum;
                            $tag.css({
                                marginTop: tempNum + 'px'
                            });
                            moveTarget = '';

                            timer = setTimeout(function () {
                                self.play()
                            }, options.interval)
                        });
                };
                var _down = function () {

                    var tagLi = $tag.find('li:last');
                    var tagNum = parseInt(tagLi.height() + tagLi.pxToNum('padding-top') + tagLi.pxToNum('padding-bottom'));
                    var moveTime = options.duration;
                    if (moveTarget === '' || moveTarget === null || moveTarget === undefined) {
                        moveTarget = $tag.pxToNum('margin-top') + tagNum + 'px'
                    } else {
                        var newTagNum = moveTarget.substring(0, moveTarget.length - 2);
                        moveTime = options.duration / tagNum * Math.abs($tag.pxToNum('margin-top') - newTagNum);
                    }
                    $tag.animate({
                            marginTop: moveTarget //滚动事件
                        }, moveTime,
                        function () {
                            //滚动后移动被滚出屏幕的li
                            $tag.find('li:last').prependTo($tag);
                            var tempDis = $tag.pxToNum('margin-top');
                            var tempNum = tempDis - tagNum;
                            $tag.css({
                                marginTop: tempNum + 'px'
                            });
                            moveTarget = '';

                            timer = setTimeout(function () {
                                self.play()
                            }, options.interval)
                        });
                };

                var _left = function () {
                    var tagLi = $tag.find('li:first');
                    var tarNum = parseInt(tagLi.width() + tagLi.pxToNum('padding-left') + tagLi.pxToNum('padding-right'));
                    var moveTime = options.duration;
                    if (moveTarget === '' || moveTarget === null || moveTarget === undefined) {
                        moveTarget = $tag.pxToNum('margin-left') - tarNum + 'px'
                    } else {
                        var newTagNum = moveTarget.substring(0, moveTarget.length - 2);
                        moveTime = options.duration / tarNum * Math.abs($tag.pxToNum('margin-left') - newTagNum);
                    }
                    $tag.animate({
                            marginLeft: moveTarget //滚动事件
                        }, moveTime,
                        function () {
                            //滚动后移动被滚出屏幕的li
                            tagLi.appendTo($tag);
                            var tempDis = $tag.pxToNum('margin-left');
                            var tempNum = tempDis + tarNum;
                            $tag.css({
                                marginLeft: tempNum + 'px'
                            });
                            moveTarget = '';

                            timer = setTimeout(function () {
                                self.play()
                            }, options.interval)
                        });
                };

                var _right = function () {
                    var tagLi = $tag.find('li:last');
                    var tarNum = parseInt(tagLi.width() + tagLi.pxToNum('padding-left') + tagLi.pxToNum('padding-right'));
                    var moveTime = options.duration;
                    if (moveTarget === '' || moveTarget === null || moveTarget === undefined) {
                        moveTarget = $tag.pxToNum('margin-left') + tarNum + 'px'
                    } else {
                        var newTagNum = moveTarget.substring(0, moveTarget.length - 2);
                        moveTime = options.duration / tarNum * Math.abs($tag.pxToNum('margin-left') - newTagNum);
                    }
                    $tag.animate({
                            marginLeft: moveTarget //滚动事件
                        }, moveTime,
                        function () {
                            //滚动后移动被滚出屏幕的li
                            tagLi.prependTo($tag);
                            var tempDis = $tag.pxToNum('margin-left');
                            var tempNum = tempDis - tarNum;
                            $tag.css({
                                marginLeft: tempNum + 'px'
                            });
                            moveTarget = '';

                            timer = setTimeout(function () {
                                self.play()
                            }, options.interval)
                        });
                };

                if (options.axis == 'up') {
                    _up();
                } else if (options.axis == 'down') {
                    _down();
                } else if (options.axis == 'left') {
                    _left();
                } else if(options.axis == 'right'){
                    _right();
                }
            };
            this.stop = function () {
                clearTimeout(timer);
                if ($tag.is(':animated')) $tag.stop();
            }

        }

        return this.each(function () {
            var slide = new nc_scroll($(this));
            setTimeout(function () {
                slide.play();
            }, options.interval);
        })

    };

    $.fn.pxToNum = function (str) {

        return parseInt($(this).css(str).substring(0, $(this).css(str).length - 2));

    }

})($);