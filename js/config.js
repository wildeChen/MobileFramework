/**
 * @namespace config
 */
define(function () {
    var DOM = {
        HEAD:'head',
        FOOT:'foot',
        MAIN:'main'
    };

    /**
     * @name config
     * @example
     * 'home':{
     *   address:'js/model/home.html',
     *   renderer:DOM.MAIN,
     *   js:'../js/page/page.home'
     * }
     */
    return {
        'home':{
            address:'js/model/home.html',
            renderer:DOM.MAIN,
            js:'../js/page/page.home'
        },
        'fantasy':{
            address:'js/model/fantasy.html',
            renderer:DOM.MAIN,
            js:'../js/page/page.fantasy'
        },

        'head':{
            address:'js/model/head.html',
            renderer:DOM.HEAD,
            js:'js/page/home'
        },

        'foot':{
            address:'js/model/foot.html',
            renderer:DOM.FOOT,
            js:'js/page/home'
        },

        'example':{
            address:'js/model/foot.html',
            depend:'home',
            nexus:['head','foot'],
            renderer:DOM.FOOT,
            js:'js/page/home'
        }
    }
});